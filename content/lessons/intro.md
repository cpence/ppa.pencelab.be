---
title: Teaching Resources
subhead: A collection of resources for teaching the history of biology and the nature of science to secondary or early bachelor's students.
type: blog
---

{{<lessons>}}

Students often enter our classrooms with a variety of fairly simple misunderstandings of evolutionary theory, absorbed from popular culture or as collateral damage from the battle between science and religion. Sometimes, dispelling these misconceptions can be a significant challenge, and can leave students feeling as if they've failed to "get" something obvious.

But when we turn to the history of biology, what we find is that many of the same kinds of errors that our students express are precisely those that characterized the period surrounding Darwin's introduction of evolution. To put it simply, evolution is a difficult idea to master, and our students are making not silly or stupid mistakes, but the very same mistakes made by professional, practicing scientists! My aim here is to provide you with a handful of pre-packaged examples of how to integrate instances of these misunderstandings into your classroom, including relevant primary source excerpts.

This series of lessons is designed to address five such cases using real historical episodes -- simultaneously introducing content knowledge about evolutionary theory, historical context, as well as engaging questions in the nature of science, now increasingly important in contemporary science-education standards. The broad format of each lesson follows the example found in Douglas Allchin's [_Teaching the Nature of Science: Perspectives & Resources,_](https://www.amazon.com/Teaching-Nature-Science-Perspectives-Resources/dp/098925240X) which masterfully details some of the ways in which material in the history and philosophy of science can shed light on NOS education.

In short, then, these lessons cover the following student misunderstandings:

- The history of evolution is a steady progress toward _more perfect_ or _more complex_ organisms, with humans as the pinnacle
  - We'll look at _Darwin's own_ struggle with exactly this question -- while he repeatedly claimed that we ought not describe evolutionary advance in terms of progress, he nonetheless regularly slipped back into progress-based language.
- Evolutionary theory isn't _truly scientific,_ because it doesn't use _the scientific method_
  - This was a critique raised against Darwin himself, notably by the nineteenth-century philosopher _John Herschel._
- Organisms evolve by _striving for success,_ and pass the results of that striving to their offspring
  - This _Lamarckian_ view was advocated by a number of biologists in the late-nineteenth and early-twentieth centuries.
- Evolution isn't always about adaptation, sometimes it can _get stuck,_ creating non-adaptive features
  - Perhaps more a historical curiosity than a common misunderstanding, the theory of _orthogenesis_ proposed precisely this, which proves to be an interesting way to examine evolutionary adaptations and non-adaptive characters.
- Mendel's work in genetics means that _traits are "binary,"_ either "on" or "off" as we might draw in a Punnett square
  - Even the very traits that Mendel himself studied in peas are more complex than they first appear, and _W. F. R. Weldon_ argued early in the days of genetics that environmental influence is important.

In the lessons that follow, you will find historical vignettes, with images and suggested readings. Those vignettes are punctuated by "THINK" questions designed to encourage reflection on nature-of-science themes, and conclude with an explicit NOS reflection question to support direct discussion of major NOS topics.

## Downloadable PDFs

- **Lesson 1**
  - [Color, letter-size paper](/downloads/Lesson1-Color-Letter.en.pdf)
  - [Color, A4-size paper](/downloads/Lesson1-Color-A4.en.pdf)
  - [Black-and-white, letter-size paper](/downloads/Lesson1-BW-Letter.en.pdf)
  - [Black-and-white, A4-size paper](/downloads/Lesson1-BW-A4.en.pdf)
- **Lesson 2**
  - [Color, letter-size paper](/downloads/Lesson2-Color-Letter.en.pdf)
  - [Color, A4-size paper](/downloads/Lesson2-Color-A4.en.pdf)
  - [Black-and-white, letter-size paper](/downloads/Lesson2-BW-Letter.en.pdf)
  - [Black-and-white, A4-size paper](/downloads/Lesson2-BW-A4.en.pdf)
- **Lesson 3**
  - [Color, letter-size paper](/downloads/Lesson3-Color-Letter.en.pdf)
  - [Color, A4-size paper](/downloads/Lesson3-Color-A4.en.pdf)
  - [Black-and-white, letter-size paper](/downloads/Lesson3-BW-Letter.en.pdf)
  - [Black-and-white, A4-size paper](/downloads/Lesson3-BW-A4.en.pdf)
- **Lesson 4**
  - [Color, letter-size paper](/downloads/Lesson4-Color-Letter.en.pdf)
  - [Color, A4-size paper](/downloads/Lesson4-Color-A4.en.pdf)
  - [Black-and-white, letter-size paper](/downloads/Lesson4-BW-Letter.en.pdf)
  - [Black-and-white, A4-size paper](/downloads/Lesson4-BW-A4.en.pdf)
- **Lesson 5**
  - [Color, letter-size paper](/downloads/Lesson5-Color-Letter.en.pdf)
  - [Color, A4-size paper](/downloads/Lesson5-Color-A4.en.pdf)
  - [Black-and-white, letter-size paper](/downloads/Lesson5-BW-Letter.en.pdf)
  - [Black-and-white, A4-size paper](/downloads/Lesson5-BW-A4.en.pdf)
- **Teacher's Guide**
  - [Letter-size paper](/downloads/Guide-Letter.en.pdf)
  - [A4-size paper](/downloads/Guide-A4.en.pdf)

## Thanks

Many thanks to [John S. Wilkins](https://evolvingthoughts.net/) for help fact-checking the historical claims throughout the lessons. I owe much of the general structure of Lesson 5 to Greg Radick's approach to the Weldon-Mendel relationship, though I don't presume that he would agree with everything in my presentation there. Some of the material here dates from discussions that I had about these questions with my colleagues and friends [Greg Macklem](https://stemeducation.nd.edu/directory/faculty/greg-macklem) and [Erik Peterson,](https://history.ua.edu/people/erik-peterson/) building on work that we'd done for a presentation at [the NSTA's 2017 New Orleans meeting.](http://archive.charlespence.net/talks/2017_Darwin_NSTA.pdf) Finally, I owe some of my perspective on these issues to my years teaching in the [LSU GeauxTeach program in the College of Science.](https://www.lsu.edu/science/geauxteach_sci/)

This material was prepared in part with funding from the US National Science Foundation, under HPS Scholars Award #1826784.

## License

The text of the lessons is available open-access, under the [Creative Commons CC-BY license.](https://creativecommons.org/licenses/by/4.0/) Images contained within the lessons are available under their own licenses, which are described in the lessons themselves. Note that the French translations are available not under CC-BY, but [CC-BY-NC-SA.](https://creativecommons.org/licenses/by-nc-sa/4.0/) All source files for the lessons are available [on Codeberg,](https://codeberg.org/cpence/ppa-teaching) with DOI [10.5281/zenodo.4704737](https://doi.org/10.5281/zenodo.4704737), or on the [UCLouvain OER](https://hdl.handle.net/20.500.12279/802).

## French Translation

These lessons are also available in French, thanks to the generous support of the [Fonds de la Recherche Scientifique - FNRS,](https://www.fnrs.be) grant number F.4526.19, and the "Digital University" program of UCLouvain. They were translated by [Sandra Mouton.](http://www.sandramouton.com/) To switch to the French-language version, [click here.](/cours/intro)
