document.addEventListener("DOMContentLoaded", function () {
  // Go through every <img> element, take the classes that we put on those
  // in Pandoc, and apply them with a '-figure' suffix to their containing
  // <figure> elements. Thanks to a GH issue for the snippet here.
  var figures = Array.prototype.slice
    .call(document.getElementsByClassName("figure"), 0)
    .concat(
      Array.prototype.slice.call(document.getElementsByTagName("figure"), 0)
    );

  for (var i = 0; i < figures.length; i++) {
    var figure = figures[i];
    var imgs = figure.getElementsByTagName("img");
    if (imgs.length > 0) {
      classes = imgs[0].classList;
      for (var j = 0; j < classes.length; j++) {
        figure.classList.add(classes[j] + "-figure");
      }
    }
  }
});
